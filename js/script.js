let menu = document.querySelector('#menu-bar');
let navbar = document.querySelector('.navbar');

menu.onclick =() =>{
    menu.classList.toggle('fa-times');
    navbar.classList.toggle('active');
}

let slides = document.querySelectorAll('.slide-container');
let index = 0;

function next(){
    slides[index].classList.remove('active');
    index = (index + 1) % slides.length;
    slides[index].classList.add('active');
}

function prev(){
    slides[index].classList.remove('active');
    index = (index - 1 + slides.length) % slides.length;
    slides[index].classList.add('active');
}

document.querySelectorAll('.featured-image-1').forEach(image_1 =>{
    image_1.addEventListener('click', () =>{
        var src = image_1.getAttribute('src');
        document.querySelector('.big-image-1').src = src;
    });
});

document.querySelectorAll('.featured-image-2').forEach(image_2 =>{
    image_2.addEventListener('click', () =>{
        var src = image_2.getAttribute('src');
        document.querySelector('.big-image-2').src = src;
    });
});

document.querySelectorAll('.featured-image-3').forEach(image_3 =>{
    image_3.addEventListener('click', () =>{
        var src = image_3.getAttribute('src');
        document.querySelector('.big-image-3').src = src;
    });
});















function add_to_table(hour, minute, period, i) {
	var table = document.getElementById("times");
	var row = table.insertRow();
	var cell = row.insertCell(0);
	var text1 = hour + ":" + ("00" + minute).slice(-2) + " " + period;
	if (i + 1 == 6) {
		text1 += " (Sugerido)";
		cell.style.backgroundColor = "gold";
	}
	var text2 = (i + 1) * 1.5 + " horas de sueño";
	var text3 = (i + 1) + " ciclos de sueño";
	cell.innerHTML = text1 + "<br><div class='small_text'>" + text2 + ', ' + text3 + "</div>";
}

function calculate() {
	var hour = parseInt(document.getElementById("hour").value);
	var minute = parseInt(document.getElementById("minute").value);
	var period = document.getElementById("period").value;

	if (isNaN(hour) || isNaN(minute) || period == "") {
		document.getElementById("instruction").innerHTML = "Selecciona hora, minutos y AM/PM";
		return;
	}

	document.getElementById("times").innerHTML = "";


	if (document.getElementById("choice").value == "Quiero despertar a las:") {
		document.getElementById("instruction").innerHTML = "Sugerimos dormir a las: ";

		for (var i = 0; i < 16; i++) {
			var pastHour = hour

			if (minute < 30) {
				hour -= 2;
				minute += 30
			} else {
				hour -= 1;
				minute -= 30;
			}

			if (hour < 1 && pastHour > 0) {
				period = (period == "AM") ? "PM" : "AM";
			}

			if (hour < 1) {
				hour += 12;
			}

			add_to_table(hour, minute, period, i);
		}
	} else {
		document.getElementById("instruction").innerHTML = "Pon tu alarma una de estas horas: ";

		for (var i = 0; i < 16; i++) {
			var pastHour = hour;

			if (minute < 30) {
				hour += 1;
				minute += 30;
			} else {
				hour += 2;
				minute -= 30
			}

			if (hour > 11 && pastHour < 12) {
				period = (period == "AM") ? "PM" : "AM";
			}

			if (hour > 12) {
				hour -= 12;
			}

			add_to_table(hour, minute, period, i);
		}
	}
}